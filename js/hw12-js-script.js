const diameterInput = document.createElement("input");
const colorInput = document.createElement("input");
const buttonDraw = document.createElement("button");

document.getElementById("draw-circles").onclick = () => {

    diameterInput.placeholder = "Enter the diameter in px";
    colorInput.placeholder = "Enter the color, like yellow";
    
    // buttonDraw.id = "add-circles";
    buttonDraw.innerText = "Let's draw";

    diameterInput.style.backgroundColor = "yellow"; // стилізували diameterInput
    diameterInput.style.boxShadow = "3px 3px 15px grey";
    diameterInput.style.margin = "25px";
    colorInput.style.backgroundColor = "yellow"; // стилізували colorInput
    colorInput.style.boxShadow = "3px 3px 15px grey";
    colorInput.style.margin = "25px";
    buttonDraw.style.backgroundColor = "dodgerblue"; // стилізували buttonDraw
    buttonDraw.style.borderRadius = "3px";
    buttonDraw.style.padding = "5px 25px";
    buttonDraw.style.color = "bisque";
    buttonDraw.style.fontFamily = "sans-serif";
    buttonDraw.style.fontSize = "14px";
    buttonDraw.style.boxShadow = "3px 3px 15px grey";

    document.body.appendChild(diameterInput); // додали на сторінку diameterInput
    document.body.appendChild(colorInput); // додали на сторінку colorInput
    document.body.appendChild(buttonDraw); // додали на сторінку кнопку buttonDraw

    document.body.removeChild(document.getElementById("draw-circles")); // видалили кнопку з id "draw-circle"
};

buttonDraw.onclick = function (e) {
    console.log("diameter - ", diameterInput.value); // перевірка значення введеного діаметра

    const circle = document.createElement( "div" ); // створили блок

    circle.style.width = diameterInput.value + "px"; // стилізація блоку: введене значення користувачем
    circle.style.height = diameterInput.value + "px"; // стилізація блоку: введене значення користувачем
    circle.style.borderRadius = diameterInput.value + "px"; // стилізація блоку: введене значення користувачем
    circle.style.backgroundColor = colorInput.value;
    circle.style.border = "2px solid black";
    circle.style.margin = "100px auto";

    document.body.appendChild(circle);
};